import React, { Component } from 'react'
import axios from 'axios'

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class Issuers extends Component {
  state = {
    issuers: []
  }

  async componentDidMount() {
    try {
      const data = await axios.get('http://localhost:3000/issuers')

      this.setState({
        issuers: data.data
      });
    } catch(e) {
      this.setState({error: "sumpin f'd up"})
    }
  }

  render () {
    return (
      <Card>
        <CardContent>
          <Typography gutterBottom variant="headline" component="h2">
            Issuers:
          </Typography>

          <List component="nav">
            { this.state.issuers.map(i => <Issuer key={i} name={i.name} />) }
          </List>
        </CardContent>
      </Card>
    )
  }
}

const Issuer = (props) => (
  <ListItem button>
    <ListItemText primary={props.name} />
  </ListItem>
)

export default Issuers
