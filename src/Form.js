import React, { Component } from 'react'

class Form extends Component {
  state = {
    issuerName: ""
  }

  handleChange (e) {
    this.setState({ issuerName: e.target.value });
  }

  handleSubmit (e) {
    e.preventDefault();
    console.log("state: ", JSON.stringify(this.state));
  }

  render () {
    const { issuerName } = this.state;

    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <p>Issuer Name: <input type="text" name="name" value={issuerName} onChange={this.handleChange.bind(this)} /></p>
        <button type="submit">Save</button>
      </form>

    )
  }
}

export default Form;
