import React, { Component, Fragment } from 'react';
import './App.css';

import Home from './Home';
import Issuers from './Issuers';
import Form from './Form';

import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

const HomeLink = props => <Link to="/" {...props} />
const IssuersLink = props => <Link to="/issuers" {...props} />
const FormLink = props => <Link to="/form" {...props} />

class App extends Component {
  render() {
    return (
      <Fragment>
        <CssBaseline />

        <Router>
          <div style={{flexGrow: 1}}>
            <AppBar position="static">
              <Toolbar>
                <Button component={HomeLink}>Home</Button>
                <Button component={IssuersLink}>Issuers</Button>
                <Button component={FormLink}>Form</Button>
              </Toolbar>
            </AppBar>

            <Route exact path="/" component={Home}/>
            <Route path="/issuers" component={Issuers}/>
            <Route path="/form" component={Form}/>
          </div>
        </Router>
      </Fragment>
    );
  }
}

export default App;
